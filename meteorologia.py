from sklearn.naive_bayes import GaussianNB
import numpy as np

sol = 1
nublado = 2
chuva = 3

sim = 4
nao = 5

x = np.array([[1,5],[2,4], [3,4], [1,4], [1,4], [2,4], [3,5], [3,5], [1,4], [3,4], [1,5], [2,4], [2,4], [3,5]])
y = np.array([1, 2, 3, 1, 1, 2, 3, 3, 1, 3, 1, 2, 2, 3])

model = GaussianNB()

model.fit(x,y)

predicted = model.predict([[1,5],
                           [2,4],
                           [1,4],
                           [2,4],
                           [3,5]])

print(predicted)